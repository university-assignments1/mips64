.data

	image_in:
		.space 16
		.byte	0	,	0	,	0	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
		.byte	11	,	12	,	13	,	0	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7
		.byte	8	,	9	,	10	,	11	,	12	,	0	,	0	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4
		.byte	5	,	6	,	7	,	8	,	9	,	10	,	11	,	0	,	0	,	242	,	0	,	255	,	0	,	255	,	0	,	1
		.byte	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	0	,	0	,	243	,	242	,	0	,	255	,	0
		.byte	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	0	,	0	,	244	,	243	,	242
		.byte	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	0	,	0	,	245
		.byte	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	0
		.byte	0	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5
		.byte	6	,	0	,	0	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2
		.byte	3	,	4	,	5	,	0	,	0	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255
		.byte	0	,	1	,	2	,	3	,	4	,	0	,	0	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0
		.byte	255	,	0	,	255	,	0	,	1	,	2	,	3	,	0	,	0	,	250	,	249	,	248	,	247	,	246	,	245	,	244
		.byte	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	0	,	0	,	251	,	250	,	249	,	248	,	247
		.byte	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	0	,	0	,	252	,	251	,	250
		.byte	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	0	,	0	,	253
		.byte	252	,	251	,	250	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0
		.byte	0	,	254	,	253	,	252	,	251	,	250	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255
		.space 20

	image_out: .space 256

.code

	# R1: current address of image_in in memory
	daddi R1, $zero, image_in
	# R2: end address (of 16x16 image) in memory (DO NOT TOUCH IT)
	daddi R2, R1, 307	# R2 = image_in + 18*17 + 1
	daddi R1, R1, 19

	# R9: current address of image_out in memory
	daddi R9, $zero, image_out

	# R13: counter for each line (on 16x16 image)
	daddi R13, $zero, 14

	# Constant
	daddi R20, $zero, 255

	##########################
	# calculating the result
	Loop1:

		# check for end of image
		beq R1, R2, Exit

		##########################
		#LOOP UNROLL 1
		lbu R3, -19(R1)
		lbu R7, -17(R1)
		lbu R6, -18(R1)

		dsub R12, R7, R3
		dsll R12, R12, 1
		dadd R3, R12, R6		
		##########################

		##########################
		#LOOP UNROLL 2
		lbu R8, -1(R1)
		lbu R15, 1(R1)
		lbu R14, 0(R1)

		dsub R12, R15, R8
		dsll R12, R12, 2
		dadd R12, R12, R14
		dadd R3, R3, R12
		##########################

		##########################
		#LOOP UNROLL 3
		lbu R16, 17(R1)
		lbu R19, 19(R1)
		lbu R18, 18(R1)

		dsub R12, R19, R16
		dsll R12, R12, 1
		dadd R12, R12, R18
		dadd R3, R3, R12
		##########################

		##########################
		# Correcting the result
		slti R17, R3, 1 # if R3<=0 R17=1 else R17=0
		movn R3, $zero, R17

		slti R17, R3, 255	# if R3<255 R17=1 else R17=0
		movz R3, R20, R17
		##########################

		##########################
		# store the result to the image_out
		sb R3, 0(R9)
		##########################


		##########################
		# LOOP1 UNROLL 1
		##########################
		#LOOP UNROLL 1
		lbu R3, -16(R1)
		lbu R8, 2(R1)
		lbu R16, 20(R1)

		dsub R12, R3, R6
		dsll R12, R12, 1
		dadd R3, R12, R7
		##########################

		##########################
		#LOOP UNROLL 2
		dsub R12, R8, R14
		dsll R12, R12, 2
		dadd R12, R12, R15
		dadd R3, R3, R12
		##########################

		##########################
		#LOOP UNROLL 3
		dsub R12, R16, R18
		dsll R12, R12, 1
		dadd R12, R12, R19
		dadd R3, R3, R12
		##########################

		##########################
		# Correcting the result
		slti R17, R3, 1 # if R3<=0 R17=1 else R17=0
		movn R3, $zero, R17

		slti R17, R3, 255	# if R3<255 R17=1 else R17=0
		movz R3, R20, R17
		##########################

		##########################
		# store the result to the image_out
		sb R3, 1(R9)
		daddi R9, R9, 2
		##########################

		# check for end of line
		beqz R13, reset
		daddi R1, R1, 2
		j Loop1
		daddi R13, R13, -2

		reset:
			daddi R1, R1, 2
			j Loop1
			daddi R13, $zero, 14

	Exit:
	halt
		