#include <stdio.h>

#define WIDTH	8
#define HEIGHT	8

void main(void)
{
	int i, j, result;
	int image_in[WIDTH+2][HEIGHT+2] = {
		{0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0},
		{0	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	0},
		{0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	0},
		{0	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	0},
		{0	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	0},
		{0	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	0},
		{0	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	0},
		{0	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0},
		{0	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	0},
		{0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0}
	};
	int image_out[WIDTH][HEIGHT];

	for(i=1; i < HEIGHT+1; i++) {
		for(j=1; j < WIDTH+1; j++) {
			result = -2*image_in[i-1][j-1] +
				1*image_in[i-1][j] +
				2*image_in[i-1][j+1] +
				-4*image_in[i][j-1] +
				1*image_in[i][j] +
				4*image_in[i][j+1] +
				-2*image_in[i+1][j-1] +
				1*image_in[i+1][j] +
				2*image_in[i+1][j+1];

			if(result < 0) image_out[i-1][j-1] = 0;
			else if(result > 255) image_out[i-1][j-1] = 255;
			else image_out[i-1][j-1] = result;
		}
	}

	for(i=0; i < HEIGHT; i++) {
		for(j=0; j < WIDTH; j++) {
			printf("%02x ", image_out[i][j]);
		}
		printf("\n");
	}

}
