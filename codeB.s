.data

	image_in:
		.space 16
		.byte	0	,	0	,	0	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
		.byte	11	,	12	,	13	,	0	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7
		.byte	8	,	9	,	10	,	11	,	12	,	0	,	0	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4
		.byte	5	,	6	,	7	,	8	,	9	,	10	,	11	,	0	,	0	,	242	,	0	,	255	,	0	,	255	,	0	,	1
		.byte	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	0	,	0	,	243	,	242	,	0	,	255	,	0
		.byte	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	0	,	0	,	244	,	243	,	242
		.byte	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	0	,	0	,	245
		.byte	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	0
		.byte	0	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	3	,	4	,	5
		.byte	6	,	0	,	0	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2
		.byte	3	,	4	,	5	,	0	,	0	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255
		.byte	0	,	1	,	2	,	3	,	4	,	0	,	0	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0
		.byte	255	,	0	,	255	,	0	,	1	,	2	,	3	,	0	,	0	,	250	,	249	,	248	,	247	,	246	,	245	,	244
		.byte	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	2	,	0	,	0	,	251	,	250	,	249	,	248	,	247
		.byte	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	1	,	0	,	0	,	252	,	251	,	250
		.byte	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0	,	0	,	0	,	253
		.byte	252	,	251	,	250	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255	,	0	,	255	,	0
		.byte	0	,	254	,	253	,	252	,	251	,	250	,	249	,	248	,	247	,	246	,	245	,	244	,	243	,	242	,	0	,	255
		.space 20

	image_out: .space 256

	kernel: .byte 1, 2, 1

.code

	# R1: current address of image_in in memory
	# R2: end address (of 16x16 image) in memory
	# R9: current address of image_out in memory
	# R13: counter for each line (on 16x16 image)

	daddi R1, $zero, image_in
	daddi R2, R1, 307	# R2 = image_in + 18*17 + 1
	daddi R1, R1, 19

	daddi R13, $zero, 15

	# Constant
	daddi R20, $zero, 255

	##########################
	# calculating the result
	Loop1:

		# check for end of image
		beq R1, R2, Exit
		#nop
		daddi R4, $zero, 3	# k=3

		daddi R11, R1, -18
		daddi R3, $zero, 0

		# for k=3 to 0
		Loop:
			daddi R4, R4, -1	# k--

			lb R10, kernel(R4)

			lbu R5, -1(R11)
			lbu R7, 1(R11)
			lbu R6, 0(R11)

			dsub R12, R7, R5
			dsllv R12, R12, R10
			dadd R12, R12, R6
			dadd R3, R3, R12
			
		bnez R4, Loop	# Loop
		#nop
		daddi R11, R11, 18	# position+=18
		##########################

		##########################
		# Correcting the result
		slti R17, R3, 1 # if R3<=0 R17=1 else R17=0
		movn R3, $zero, R17

		slti R17, R3, 255	# if R3<255 R17=1 else R17=0
		movz R3, R20, R17
		##########################

		##########################
		# store the result to the image_out
		sb R3, image_out(R9)
		daddi R9, R9, 1
		##########################

		# check for end of line
		beqz R13, reset
		#nop
		daddi R1, R1, 1
		j Loop1
		#nop
		daddi R13, R13, -1		

		reset:
			daddi R13, $zero, 15			
			j Loop1
			#nop
			daddi R1, R1, 2

	Exit:
	halt
